module.exports = {
  options: {
    autoprefixer: {
      browsers: ['last 2 versions']
    }
  },
  plugins: [
    require('autoprefixer')
  ]
}
