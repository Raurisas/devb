var EntryPoint =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _main = __webpack_require__(0);

var _main2 = _interopRequireDefault(_main);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formIsValid = true;

module.exports = {
    validateInput: function validateInput(element) {
        _validateInput(element, validators.isElementEmpty);
    },
    submitForm: function submitForm(form) {
        formIsValid = true;
        var nameElement = document.getElementById("name");
        var lastNameElement = document.getElementById("lastName");
        var messageElement = document.getElementById("message");

        _validateInput(nameElement, validators.isElementEmpty);
        _validateInput(lastNameElement, validators.isElementEmpty);
        _validateInput(messageElement, validators.isElementEmpty);

        if (formIsValid) {
            alert('Form submitted succesfully');
            return;
        }
        return false;
    }
};

function _validateInput(element, validator) {
    if (validator(element)) {
        formIsValid = false;
        errorHandling.addErrorClass(element);
    } else {
        errorHandling.removeErrorClass(element);
    }
}

function addErrorClass(element) {
    element.classList.add('error');
}

function removeErrorClass(element) {
    element.classList.remove('error');
}

function isElementEmpty(element) {
    return !element.value;
}

//All validators should go here
var validators = {
    isElementEmpty: isElementEmpty
};

var errorHandling = {
    addErrorClass: addErrorClass,
    removeErrorClass: removeErrorClass
};

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map